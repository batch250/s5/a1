import com.zuitt.example.*;

import java.util.ArrayList;

public class Main {



    public static void main(String[] args) {

        //instantiated a Phonebook
        Phonebook myPhonebook = new Phonebook();


        //instantiated a Contact
        Contact favoritePerson = new Contact();
//        favoritePerson.setName("Mocha");
        favoritePerson.setContactNum("09214522");
        favoritePerson.setAddress("");


        //instantiated a Contact
        Contact dontAnswer = new Contact();
        dontAnswer.setName("Elmer");
        dontAnswer.setContactNum("214Ihate");
        dontAnswer.setAddress("Dolores, Quezon");

        //new contacts
        ArrayList<Contact> newContacts = new ArrayList<>();
        newContacts.add(favoritePerson);
        newContacts.add(dontAnswer);


        //add to phonebook
        myPhonebook.setContacts(newContacts);

        myPhonebook.getContacts();
        //print info function
        myPhonebook.printInfo(favoritePerson);

        myPhonebook.call(favoritePerson);

        myPhonebook.text("Punta ka dito dala ka foods", favoritePerson);
    }

}