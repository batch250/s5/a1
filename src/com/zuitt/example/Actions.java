package com.zuitt.example;

public interface Actions {

    void call(Contact contact);

    void text(String message, Contact contact);

}
