package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;

public class Phonebook implements Actions{


    ArrayList<Contact> contacts = new ArrayList<>();


    public Phonebook(){
//        this.contacts = new ArrayList<Contact>(Arrays.asList("Jane", "1234560", "New York"));
    }



    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }


    public ArrayList<Contact> getContacts(){
        System.out.println("PHONEBOOK");
        System.out.println("Registered Contacts: "+this.contacts.size());
        if (this.contacts==null || this.contacts.size()<1){
            System.out.println("Phonebook is currently empty.");
        }
        else{
            this.contacts.forEach(contact -> {
                System.out.println("***************************");
                System.out.println("Name: " + contact.getName());
                System.out.println("Contact Number: " + contact.getContactNumber());
                System.out.println("Address: "+ contact.getAddress());
                System.out.println("***************************");
            });
        }
        
        return this.contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {

        this.contacts = contacts;
    }

    public void call(Contact contact){
        System.out.println("Calling "+contact.getName());
    }

    public void text(String message, Contact contact){
        System.out.println("Sent To: "+contact.getName());
        System.out.println("==============");
        System.out.println('"'+message+'"');
    }

    public  void printInfo(Contact contact){

        if (contact.getName() !=null && contact.getName()!="" ){
            System.out.println("Printed info for: "+contact.getName());
            System.out.println(contact.getName());
            System.out.println("--------------------------");
            if(contact.getContactNumber()!=null && contact.getContactNumber()!="" ){
                System.out.println(contact.getName()+" has the following registered number: ");
                System.out.println(contact.getContactNumber());
            }else{
                System.out.println(contact.getName()+" has NO registered number!");
            }
            if(contact.getAddress()!=null && contact.getAddress()!=""){
                System.out.println(contact.getName()+" has the following registered address: ");
                System.out.println(contact.getAddress());
            }else{
                System.out.println(contact.getName()+" has NO registered address!");
            }
            System.out.println("--------------------------");
        }
        else{
            System.out.println("Printed info: ");
            System.out.println("No Contact.");
        }



    }


}
