package com.zuitt.example;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Contact  {

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();

    //Declaring properties

    private String name;
    private String contactNumber;
    private String address;




    //empty constructor
    public Contact (){


    }

    //parameterized Constructor
    public Contact (String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }




    //Getter and Setter Functions

    public String getName(){
        return this.name;
    }

    public String getContactNumber(){
        return this.contactNumber;
    }

    public String getAddress(){
        return this.address;
    }



    //Setters

    public void setName(String name){
        this.name = name;
    }

    public void setContactNum(String contactNumber){
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address){
        this.address = address;
    }



    public void call(){
        System.out.println("Calling "+ this.name + " ....");
    }

    public void text(String message){
        System.out.println("Sent To: "+this.name);
        System.out.println("message: "+'"'+message+'"');
        System.out.println(dtf.format(now));
    }

}
